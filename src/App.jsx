import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Example from './components/Example';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Example} />
    </Switch>
  </BrowserRouter>
);

export default App;
